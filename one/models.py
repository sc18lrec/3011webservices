from django.db import models

# Create your models here.


class ModuleReference(models.Model):
    module_code = models.CharField('Module Code', max_length=3, unique=True)
    module_name = models.CharField('Module Name', max_length=200)

    def __str__(self):
        x = self.module_code + ', ' + self.module_name
        return x

class Professor(models.Model):
    prof_code = models.CharField('Professor Code', max_length=3, unique=True)
    prof_name = models.CharField('Professor Name', max_length=200)

    def __str__(self):
        x = self.prof_code + ', ' + self.prof_name
        return x


class Modules(models.Model):
    module = models.ForeignKey(ModuleReference, on_delete=models.CASCADE)
    year = models.IntegerField('Year')
    semester = models.IntegerField('semester')
    taught_by = models.ManyToManyField(Professor)

    def __str__(self):
        t = ""
        for i in self.taught_by.all():
            t = t + str(i) + ", "
        x = str(self.module) + ', ' + str(self.year) + ', ' + str(self.semester) + ', ' + t
        return x


class Ratings(models.Model):
    prof = models.ForeignKey(Professor, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    rating = models.IntegerField('Rating out of 5')

    def __str__(self):
        x = str(self.prof) + ', ' + str(self.module) + ', ' + str(self.rating)
        return x

