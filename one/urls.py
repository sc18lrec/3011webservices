from django.urls import path, include
from rest_framework import routers
from . import views
  
router = routers.DefaultRouter()
router.register(r'past_module_instances', views.ModuleViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('register/', views.RegisterView.as_view(), name='auth_register'),
    path('login/', views.login_view),
    path('logout/', views.logout_view),
    path('prof_ratings/', views.prof_rating),
    path('rating_by_module/', views.prof_rating_by_module),
    path('rate/', views.rate)
  ]