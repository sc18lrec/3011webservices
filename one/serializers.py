from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password

from .models import ModuleReference, Modules, Professor, Ratings

class ProfessorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Professor
        fields = ('id', 'prof_code', 'prof_name')

class ModuleReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleReference
        fields = ('id', 'module_code', 'module_name')

class ModulesSerializer(serializers.ModelSerializer):
    module = ModuleReferenceSerializer()
    taught_by = ProfessorSerializer(many=True)

    class Meta:
        model = Modules
        fields = ('module', 'year', 'semester', 'taught_by')


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])

    class Meta:
        model = User
        fields = ('username', 'password', 'email')


    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()

        return user


class RatingsSerializer(serializers.ModelSerializer):
    prof = ProfessorSerializer()
    module = ModulesSerializer() 

    class Meta:
        model = Ratings
        fields = ('prof', 'module', 'rating')

class ProfRatingSerializer(serializers.ModelSerializer):
    prof = ProfessorSerializer()

    class Meta:
        model = Ratings
        fields = ('prof', 'rating')
