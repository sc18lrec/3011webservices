from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib import auth
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Avg
import decimal


from rest_framework import viewsets, generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import ModulesSerializer,RegisterSerializer
from .models import Professor, ModuleReference, Modules,  Ratings 

def myRoundUp(i):
    return int(decimal.Decimal(i).quantize(decimal.Decimal('1'), rounding=decimal.ROUND_HALF_UP))

#register new user
class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer


@csrf_exempt
@api_view(('POST',))
def login_view(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request,user)
        return HttpResponse("Successful log in", status=status.HTTP_200_OK)
    else:
        return HttpResponseBadRequest("Invalid login details")

@csrf_exempt
@api_view(('POST',))
def logout_view(request):
    auth.logout(request)
    return Response("Logged out successfully", status=status.HTTP_200_OK)

#option 1 
class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Modules.objects.all().order_by('module')
    serializer_class = ModulesSerializer

#option2
@csrf_exempt
@api_view(('GET',))
def prof_rating(request):
    list = Ratings.objects.values('prof').annotate(avg_rating=Avg('rating'))
    dict_list = []
    for i in list:
        id = i['prof']
        current_prof = Professor.objects.get(id=id)
        rating = i['avg_rating']
        new_dict = {}
        new_dict['professor'] = str(current_prof)
        new_dict['avg_rating'] = myRoundUp(rating) 
        dict_list.append(new_dict)
        print(dict_list)
    return Response(dict_list)

#option3
@csrf_exempt
@api_view(('POST', ))
def prof_rating_by_module(request):
    professor_id = request.POST.get('professor_id')
    module_code = request.POST.get('module_code')
    try:
        professor_str = str(Professor.objects.get(prof_code=professor_id))
    except:
        return Response("Professor not found", status=status.HTTP_400_BAD_REQUEST)
    try:
        module_str = str(ModuleReference.objects.get(module_code=module_code))
    except:
        return Response("Module not found", status=status.HTTP_400_BAD_REQUEST)

    ratings = Ratings.objects.values('prof', 'module').annotate(avg_rating=Avg('rating'))
    dict_list = []
    for i in ratings:
        pid = i['prof']
        mid = i['module']
        current_rating = i['avg_rating']
        current_prof = Professor.objects.get(id=pid)
        current_module_instance = Modules.objects.get(id=mid)
        current_module = current_module_instance.module
        new_dict = {}
        new_dict['professor'] = str(current_prof)
        new_dict['module'] = str(current_module)
        new_dict['avg_rating'] = myRoundUp(current_rating) 
        dict_list.append(new_dict)

    
    final_list = []
    for x in dict_list:
        rating_sum = x['avg_rating']
        rating_count = 1
        for y in dict_list:
            if x != y and x['professor']==y['professor'] and x['module']==y['module']:
                rating_sum = rating_sum + y['avg_rating']
                rating_count = rating_count + 1
                dict_list.remove(y)
        new_dict = {}
        new_dict['professor'] = x['professor']
        new_dict['module'] = x['module']
        new_dict['avg_rating'] = myRoundUp(rating_sum/rating_count)
        final_list.append(new_dict)
    
    for x in final_list:
        if x['professor']==professor_str and x['module']==module_str:
            print(x)
            return Response(x, status=status.HTTP_200_OK)
    
    return Response("No ratings for this professor on this module found.", status=status.HTTP_400_BAD_REQUEST)  

#option 4
@csrf_exempt
@api_view(('POST',))
def rate(request):
    if request.user.is_authenticated:
        prof_id = request.POST.get('professor_id')
        module_code = request.POST.get('module_code')
        year = request.POST.get('year')
        semester = request.POST.get('semester')
        rating = request.POST.get('rating')
        try:
            professor = Professor.objects.get(prof_code=prof_id)
        except:
            return(Response("Professor not found", status=status.HTTP_400_BAD_REQUEST))
        try:
            module = ModuleReference.objects.get(module_code=module_code)
            module_instance = Modules.objects.get(module=module, year=year, semester=semester)
        except:
            return(Response("Module instance not found", status=status.HTTP_400_BAD_REQUEST))

        taught = str(module_instance)
        if str(professor) in str(module_instance):
            print("Professor found")
            new_rate = Ratings(prof=professor, module=module_instance, rating=rating)
            new_rate.save()
            return Response("Rating successfully saved", status=status.HTTP_201_CREATED)
        else:        
            return Response("Professor does not teach that module instance", status=status.HTTP_400_BAD_REQUEST)


    #tell them to login first
    return Response("Please login to rate a professor", status=status.HTTP_400_BAD_REQUEST)