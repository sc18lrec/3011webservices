from django.contrib import admin
from .models import ModuleReference, Professor, Modules, Ratings

# Register your models here.

admin.site.register(ModuleReference)
admin.site.register(Professor)
admin.site.register(Modules)
admin.site.register(Ratings)
